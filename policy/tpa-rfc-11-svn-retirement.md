---
title: TPA-RFC-11: SVN retirement
approval: TPA, operations team
deadline: 2021-10-01
status: draft
discussion: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40260
affected users: operations
---

[[_TOC_]]

Summary: SVN will be retired by the end of 2021, in favor of
Nextcloud.

# Background

SVN (short for Subversion) is a version control system that is
currently used inside the Tor Project to manage private files like
contacts, accounting data, forms. It was also previously used to host
source code but that has all been archived and generally migrated to
the git service.

## Issues to be addressed

The SVN server (called `gayi`) is not very well maintained, and has
too few service admins (if any? TBD) to be considered
well-maintained. Its retirement has been explicitly called for many
times over the years: 

 * [2012: migrate SVN to git](https://gitlab.torproject.org/tpo/tpa/team/-/issues/4929)
 * [2015: shut down SVN](https://gitlab.torproject.org/tpo/tpa/team/-/issues/17202)... by 2016, no explicit solution proposed
 * [2015: move to Sparkleshare](https://gitlab.torproject.org/tpo/tpa/team/-/issues/17719)
 * [2019: move to Nextcloud](https://gitlab.torproject.org/tpo/tpa/team/-/issues/31540)
 * [2020: user survey](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2021#survey-results) (3% of respondents want to retire SVN)

An [audit of the SVN server](https://gitlab.torproject.org/tpo/tpa/team/-/issues/33537) has documented the overly [complex
access control mechanisms][] of the server as well.

[complex access control mechanisms]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/svn/#design

For all those reasons, the TPA team wishes to retire the SVN server,
as was proposed (and adopted) in the 2021 roadmap.

## Possible replacements

Many replacement services are considered for SVN:

 * git or GitLab: GitLab has private repositories and wikis, but it is
   generally considered that its attack surface is too broad for
   private content, and besides, it is probably not usable enough
   compared to the WebDAV/SVN interface currently in use
 * Nextcloud: may solve usability requirements, may have privacy
   concerns (ie. who is a Nextcloud admin?)
 * Google Docs: currently in use for some document writing because of
   limitation of the Nextcloud collaborative editor
 * Granthub: currently in use for grant writing?

## Requirements

In [issue 32273][], a set of requirements was proposed:

[issue 32273]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/32273

 * **permanence** - there should be **backups** and no data loss in
   the event of an attack or hardware failure
 * **archival** - old data should eventually be **pruned**, for
   example personal information about past employees should not be
   kept forever, financial records can be destroyed after some legal
   limit, etc.
 * **privilege separation** - some of the stuff is **private** from
   the public, or even to tor-internal members. we need to clearly
   define what those boundaries are and are strongly they need to be
   (e.g. are Nextcloud access controls? sufficient? can we put stuff
   on Google Docs? what about share.riseup.net or pad.riseup.net? etc)

# Proposal

The proposal is to retire the SVN service by December 1st 2021. All
documents hosted on the server shall be migrated to another service
before that date.

TPA suggests SVN users adopt Nextcloud as the replacement platform,
but other platforms may be used as deemed fit by the users. Users are
strongly encouraged to consult with TPA before picking alternate
platforms.

## Nextcloud access controls

A key aspect of the SVN replacement is the access controls over the
sensitive data hosted there. The [current access control
mechanisms][complex access control mechanisms] could be replicated, to
a certain extent, but probably without the web-server layer:
Nextcloud, for example, would be responsible for authentication and
not Apache itself.

The proposed access controls would include the following stakeholders:

 * "Share link": documents can be shared publicly if a user with access
   publish the document with the "Share link" feature, otherwise a
   user needs to have an account on the Nextcloud server to get access
   to any document.
 * Group and user sharing: documents can be shared with one or many
   users or groups
 * Nextcloud administrators: they can add and remove members to groups
   and can add or remove groups, those are (currently) anarcat, gaba,
   hiro, linus, and micah.
 * Sysadmins: Riseup networks manages the virtual server and the
   Nextcloud installation and has all accesses to the server.

The attack surface might be reduced (or at least shifted) by hosting
the Nextcloud instance inside TPA.

Another option might be to use [Nextcloud desktop client](https://docs.nextcloud.com/desktop/) which
supports client-side encryption, or use another client-side encryption
program. OpenPGP, for example, is broadly used inside the Tor Project
and could be used to encrypt files before they are sent to the
server. OpenPGP programs typically suffer from serious usability flaws
which may make this impractical.

## Authentication improvements

One major improvement between the legacy SVN authentication system and
Nextcloud is that the latter supports state of the art two-factor
authentication (2FA, specifically [U2F](https://en.wikipedia.org/wiki/Universal_2nd_Factor)) which allows
authentication with physical security tokens like the
[Yubikey](https://en.wikipedia.org/wiki/YubiKey).

Another improvement is that Nextcloud delegates the access controls to
non-technical users: instead of relying solely on sysadmins (which
have access anyways) to grant access, non-sysadmin users can be
granted administrator access and respond to authorization requests,
possibly more swiftly than our busy sysadmins. This also enables more
transparency and a better representation of the actual business logic
(e.g. the executive director has the authority) instead of technical
logic (e.g. the system administrator has the authority).

This also implies that Nextcloud is more transparent than the current
SVN implementation: it's easy for an administrator to see who has
access to what in Nextcloud, whereas that required a lengthy, complex,
and possibly inaccurate audit to figure out the same in SVN.

## Usability improvements

Nextcloud should be easier to use than SVN. While both Nextcloud and
SVN have desktop applications for Windows, Linux and MacOS, Nextcloud
also offers iOS (iphone) and Android apps, alongside a much more
powerful and intuitive web interface that can basically be used
everywhere.

Nextcloud, like SVN, also supports the WebDAV standard, which allows
for file transfers across a wide variety of clients and platforms.

## Migration process

SVN users would be responsible for migrating their content out of the
server. Data that would not be migrated would be lost forever, after
an extended retirement timeline, detailed below.

## Timeline

 * November 1st 2021: reminder sent to SVN users to move their data
   out.
 * December 1st 2021: SVN server (`gayi`) retired with an extra 60
   days retention period (ie. the server can be restarted easily for 2
   months)
 * ~February 1st 2022: SVN server (`gayi`) destroyed, backups kept for
   another 60 days
 * ~April 1st 2022: all SVN data destroyed

# References

 * [SVN documentation](howto/svn)
 * [issue 17202](https://gitlab.torproject.org/tpo/tpa/team/-/issues/17202): "Shut down SVN and decomission the host (gayi)",
   main ticket to track the host retirement, includes:
   * [issue 32273][]: "archive private information from SVN", includes:
     * [corpsvn data inventory](https://gitlab.torproject.org/tpo/tpa/team/-/issues/32273#note_2542833), including "currently" used file
       management tools and alternatives
   * [issue 32025][]: "Stop using corpsvn and disable it as a service"
   * [issue 40260][]: "TPA-RFC-11: SVN retirement", discussion ticket

[issue 40260]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40260
[issue 32025]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/32025
