---
title: TPA-RFC-73: Tails infra merge roadmap
costs: 
approval: 
affected users: 
deadline: 
status: draft
discussion: 
---

[[_TOC_]]

Summary: Tails infra merge roadmap.

# Background

In 2023, Tor and Tails started discussing the possibility of a merge and, in
that case, how the future of the two infrastructures would look like. The
organizational merge happened in July 2024 with a [rough
idea](/tpo/tpa/team/-/wikis/roadmap/tails-merge) of the several components that
would have to be taken care of and the clarity that merging infrastructures
would be a several-years plan. This document intends to build on the work
previously done and describe dependencies, milestones and a detailed timeline
containing all services to serve as a basis for future work.

# Proposal

## Goals

### Must have

- A list of all services with:
  - a description of the service and who are the stakehoders
  - the action to take
  - the complexity
  - a list of dependencies or blocks
  - a time estimation
- A plan to merge the Puppet codebases and servers
- A list of milestones with time estimates and and indication of ordering

### Non-Goals

- We don't aim to say exactly who will work on what and when

## Scope

This proposal is about:

- all services that the Tails Sysadmins currently maintain: each of these will
  either be kept, retired, merged with or migrated to existing TPA services
  (see the [terminology](#actions) below), depending on several factors such as
  convenience, functionality, security, etc.
- some services maintained by TPA that may act as a source or target of a
  merge, or migration.

## Terminology

### Actions

- **Keep:** Services that will be kept and maintained. They are all impacted by Puppet repo/codebase merge as their building blocks will eventually be replaced (eg. web server, TLS, etc), but they'll nevertheless be kept as fundamental for the work of the Tails Team.
- **Merge:** Services that will be kept, are already provided by Tails and TPA using the same software/system, and for which keeping only depends on migration of data and, eventually, configuration.
- **Migrate:** Services that are already provided by TPA with a different software/system and need to be migrated.
- **Retire:** Services that will be shutdown completely.

 [Keep]: #actions
 [Merge]: #actions
 [Migrate]: #actions
 [Retire]: #actions

### Complexity

- **Low:** Services that will either be **kept as is** or for which **merging with a Tor service is fairly simple**
- **Medium:** Services that require either a lot **more discussion and analysis** or **more work** than just flipping a switch
- **High:** Core services that are already **complex on one or both sides** but that we still **can't manage separately** in the long term, so we need to make some **hard choices** and **lots of work** to merge

 [Low]: #complexity
 [Medium]: #complexity
 [High]: #complexity

## Keep

### APT snapshots

**Summary:** Snapshots of the Debian archive, used for development, releases and reproducible builds.

**Stakeholders:** Tails Team

**Action:** [Keep][]

**Complexity:** [High][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:**

- [APT snapshots doc][]
- [Tagged snapshots doc][]
- [Time-based snapshots doc][]
- [Tagged snapshots][]
- [Time-based snapshots][]

 [APT snapshots doc]: https://tails.net/contribute/working_together/roles/sysadmins/services/#index1h1
 [Tagged snapshots doc]: https://tails.net/contribute/APT_repository/tagged_snapshots/
 [Time-based snapshots doc]: https://tails.net/contribute/APT_repository/time-based_snapshots/
 [Tagged snapshots]: https://time-based.snapshots.deb.tails.net
 [Time-based snapshots]: https://tagged.snapshots.deb.tails.net

### BitTorrent

**Summary:** Transmission server used to seed images.

**Stakeholders:** Tails Team

**Action:** [Keep][]

**Complexity:** [Low][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:**

- [BitTorrent][]

 [BitTorrent]: https://tails.net/contribute/working_together/roles/sysadmins/services/#index3h1

### HedgeDoc

**Summary:** Collaborative pads with several useful features out of the box.

**Stakeholders:** Tails Team

**Action:** [Keep][]

**Complexity:** [Low][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:**

- https://pad.tails.net

### ISO history

**Summary:** Archive of all Tails ISO images, useful for reproducible builds.

**Stakeholders:** Tails Team

**Action:** [Keep][]

**Complexity:** [Low][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:**

- [ISO history][]

 [ISO history]: https://tails.net/contribute/working_together/roles/sysadmins/services/#index7h1 

### Schleuder

**Summary:** Tails' and Tor's Schleuder lists.

**Stakeholders:** Tails Team, Community Council

**Action:** [Keep][]

**Complexity:** [Low][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:**

- [Schleuder][]

 [Schleuder]: https://tails.net/contribute/working_together/roles/sysadmins/services/#schleuder

### Tor Browser archive

**Summary:** Archive of Tor Browser binaries, used for development and release management.

**Stakeholders:** Tails Team

**Action:** [Keep][]

**Complexity:** [Low][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:** ∅

### Whisperback

**Summary:** Postfix Onion service used to receive bug reports sent directly from the Tails OS.

**Stakeholders:** Tails Team

**Action:** [Keep][]

**Complexity:** [Low][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:**

- [Whisperback][]

 [Whisperback]: https://tails.net/contribute/working_together/roles/sysadmins/services/#index20h1

## Merge

### APT repository

**Summary:** Contains Tails-specific packages, used for development and release management.

**Stakeholders:** Tails Team

**Action:** [Merge][]

**Complexity:** [Medium][]

**Constraints:** ∅

**References:**

- [APT repository][]

 [APT repository]: https://tails.net/contribute/APT_repository/custom/

### Authentication

**Summary:** [puppet-rbac][] for access control, users defined in hiera, mandatory 2FA for some private GitLab projects.

**Stakeholders:** TPA

**Action:** [Merge][]

- Integrate [puppet-rbac][] with:
  - Tor's LDAP
  - [Tails' GitLab configuration][]
- Implement [puppet-rbac][] in Tor's infra
- Extend the [Tails' GitLab configuration] to Tor's GitLab
- Enable 2FA requirement for relevant projects

**Complexity:** [High][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:**

- tpo/tpa/team#41839
- [puppet-rbac][]
- [Tails' GitLab configuration][]

 [Tails' GitLab configuration]: https://gitlab.tails.boum.org/tails/gitlab-config
 [puppet-rbac]: https://gitlab.tails.boum.org/tails/puppet-rbac

### Colocations

**Summary:** 

- SEACCP: 3 main physical servers (general services and Jenkins CI), USA.
- Coloclue: 2 small physical servers for backups and some redundancy, Netherlands.
- PauLLA: dev server, France.
- Puscii: VM for secondary DNS, Netherlands.
- Tachanka!: VMs for monitoring and containerized services, USA, somewhere else.

**Stakeholders:** TPA

**Action:** [Keep][]

- No big changes initially: we'll keep all current PoPs
- Credentials will be stored in the merged [Password Store](#password-store)
- Documentation and onboarding process will will be consolidated
- We'll keep a physical machine for development and testing
- Maybe retire some PoPs if they become empty with retirements/merges

**Complexity:** [Low][]

**Constraints:**

- Blocked by the merge of [Password Store](#password-store).

**References:** ∅

### Documentation

**Summary:** Public and private Sysadmins' documentation

**Stakeholders:** TPA

**Action:** [Merge][]

- Get rid of `git-remote-gcrypt`:
  - Move public info as is to the `tpo/tpa/tails/sysadmin` wiki
  - Examples of private info that should **not** be made public: `meetings/`, `planning/`, `processes/hiring
  - Archive `tpo/tpa/tails/sysadmin-private`:
    - What remains there is private history that shouldn't be publicly shared
    - The last people with access to that repo will continue to have access, as long as they still have their private keys
- Move [sysadmin doc from the Tails website](https://tails.net/contribute/working_together/roles/sysadmins/) to `tpo/tpa/tails/sysadmin`
- Rewrite what's left on the fly into Tor's doc as we merge

**Complexity:** [Low][]

**Constraints:** ∅

**References:**

- [Public documentation](https://tails.net/contribute/working_together/roles/sysadmins/)
- [Private documentation](https://gitlab.torproject.org/tpo/tpa/tails/sysadmin-private/)

### GitLab

**Summary:** Tails has a [GitLab instance](https://gitlab.tails.boum.org)
hosted by a 3rd-party. Some sysadmins' repositories have already been migrated,
at this point.

**Stakeholders:** TPA

**Action:** [Merge][]

- Not before Jan 2025 (due to Tails internal merge timeline)
- Make sure to somehow archive and not move some obsolete historic projects (eg. accounting, fundraising, summit)
- Adopt gitlabracadabra to manage Tor's GitLab

**Complexity:** [Medium][]

**Constraints:** ∅

**References:** ∅

### LimeSurvey

**Summary:** Mainly used by the UX Team.

**Stakeholders:** UX Team

**Action:** [Merge][]

**Complexity:** [Medium][]

**Constraints:** ∅

**References:**

- [LimeSurvey][]

 [LimeSurvey]: https://tails.net/contribute/working_together/roles/sysadmins/services/#index10h1

### Mailman

**Summary:** Public mailing listsm, hosted at autistici/inventati.

- amnesia-news@boum.org
- tails-dev@boum.org
- tails-testers@boum.org
- tails-l10n@boum.org

**Stakeholders:** Tails Team, Community Team

**Action:** [Merge][]

- Migrate away from the boum.org domain
- Merge into Tor's Mailman 3

**Complexity:** [Medium][]

**Constraints:** ∅

**References:**

- https://tails.net/about/contact/index.en.html#public-mailing-lists 

### MTA

**Summary:** Postfix and Schleuder

**Stakeholders:** TPA

**Action:** [Merge][]

- Merge Postfix into Tor's MTA
- [Schleuder](#schleuder) will be kept

**Complexity:** [Medium][]

**Constraints:** ∅

**References:**

- [Mail](https://tails.net/contribute/working_together/roles/sysadmins/services/#index10h1)

### Password Store

**Summary:** Password store containing Sysadmins credentials and secrets.

**Stakeholders:** TPA

**Action:** [Merge][]

**Complexity:** [Low][]

**Constraints:** ∅

**References:** ∅

### Puppet Server

**Summary:** Puppet 7, OpenPGP signed commits, published repositories, EYAML for secrets.

**Stakeholders:** TPA

**Action:** [Merge][]

**Complexity:** [High][]

**Constraints:**

- Blocked by Tor upgrade to Puppet 7
- Blocks everything we'll "keep", plus Backups, TLS, Monitoring, Firewall, Authentication

**References:** ∅

### Registrars

**Summary:** Njal.la

**Stakeholders:** TPA, Finances

**Action:** [Keep][]

- No big changes initially: we'll keep all current registrars
- Credentials will be stored in the merged [Password Store](#password-store)
- Documentation needs to be consolidated

**Complexity:** [Low][]

**Constraints:**

- Blocked by the merge of [Password Store](#password-store).

**References:** ∅

### Shifts

**Summary:** [Tails Sysadmin shifts][] and [TPA Star of the week][]

**Stakeholders:** TPA

**Action:** [Merge][]

TPA:

- [Triage](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-2-support#triage)
- [Routine tasks](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-2-support#routine)
- Interruption handling
- [Monitoring](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-33-monitoring) alerts
- [Incident response](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/incident-response)

Tails:

- Handle requests from devs
- Keep systems up-to-date, reboot when needed
- Communicate with upstream providers
- Manage GitLab: create users, update configs, process abuse reports, etc

**Complexity:** [Medium][]

**Constraints:** ∅

**References:**

- [Tails Sysadmin shifts][]
- [TPA Star of the week][]

 [Tails Sysadmin shifts]: https://tails.net/contribute/working_together/roles/sysadmins/shifts/)
 [TPA Star of the week]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-2-support#triage

### Web servers

**Summary:** Mostly Nginx (voxpupuli module) and some Apache (custom implementation)

**Stakeholders:** TPA

**Action:** [Merge][]

**Complexity:** [Medium][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:** ∅

### Security Policy

**Summary:** Ongoing adoption by TPA

**Stakeholders:** TPA

**Action:** [Merge][]

**Complexity:** [High][]

**Constraints:** ∅

**References:** tpo/tpa/team#41727

### Weblate

**Summary:** Translations are currently made by volunteers and the process is
tightly coupled with automatic updating of PO files in the Tails repository
(done by IkiWiki and custom code).

**Stakeholders:** Tails Team, Community Team

**Action:** [Merge][]

- May help mitigate certain risks (eg. [Tails Issue 20455][], [Tails Issue 20456][])
- Tor already has community and translation management processes in place
- Pending decision:
  - Option 1: Move Tor's Weblate to Tails' self-hosted instance (need to check with Tor's community/translation team for potential blockers for self-hosting)
  - Option 2: Move Tails Weblate to Tor's hosted instance (needs a plan to change the current [Translation platform design][], as it depends on Weblate being self-hosted)

**Complexity:** [High][]

**Constraints:** ∅

**References:**

- [Tails Issue 20455][]
- [Tails Issue 20456][]
- [Translation platform design][]

 [Tails Issue 20455]: https://gitlab.tails.boum.org/tails/tails/-/issues/20455
 [Tails Issue 20456]: https://gitlab.tails.boum.org/tails/tails/-/issues/20456
 [Translation platform design]: https://tails.net/contribute/design/translation_platform/

### Website

**Summary:** Lives in the main Tails repository and is built and deployed by
the GitLab CI using a patched IkiWiki.

**Stakeholders:** Tails Team

**Action:** [Merge][]

- Change deployment to the Tor's CDN
- Retire the mirror VMs in Tails infra.
- Postpone retirement of IkiWiki to a future discussion (see reference below)
- Consider splitting the website from the main Tails repository

**Complexity:** [Medium][]

**Constraints:**

- Blocks migration of DNS
- Requires po4a from Bullseye
- Requires ikiwiki from https://deb.tails.boum.org (relates to the merge of the [APT repository][])

**References:**

- https://gitlab.tails.boum.org/tails/tails/-/issues/18721
- https://gitlab.tails.boum.org/sysadmin-team/container-images/-/blob/main/ikiwiki/Containerfile

## Migrate

### Backups

**Summary:** Borg backup into an append-only Masterless Puppet client.

**Stakeholders:** TPA

**Action:** [Migrate][] one side to either Borg or Bacula

- Experiment with Borg in Tor
- Choose either Borg or Bacula and migrate everything to one of them
- Create a plan for compromised servers scenario

**Complexity:** [Medium][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).
- Blocks the migration of [Monitoring](#monitoring)

**References:**

- [Backups][]

 [Backups]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41805

### Calendar

**Summary:** Only the Sysadmins calendar is left to retire.

**Stakeholders:** TPA, Tails Team

**Action:** [Migrate][] to Nextcloud

**Complexity:** [Low][]

**Constraints:** ∅

**References:**

- tpo/tpa/team#41836

### DNS

**Summary:** PowerDNS:

- Primary: `dns.lizard`
- Secondary: `teels.tails.net` (at Puscii)
- MySQL replication
- LUA records to only serve working mirrors

**Stakeholders:** TPA

**Action:** [Migrate][]

- Migrate into a simpler design
- Migrate to either tor's configuration or, if impractical, use tails' PowerDNS as primary
- Blocked by the merge of [Puppet Server](#puppet-server).

**Complexity:** [High][]

**Constraints:**

- Blocked by the [Website](#website) merge.

**References:**

- [DNS][]

 [DNS]: https://tails.net/contribute/working_together/roles/sysadmins/services/#index4h1

### EYAML

**Summary:** Secrets are stored encrypted in EYAML files in the Tails Puppet codebase.

**Stakeholders:** TPA

**Action:** [Keep][] for now, then decide whether to [Migrate][]

- We want to have experience with both before deciding what to do

**Complexity:** [Medium][]

**Constraints:**

- Blocks the merge of [Puppet Server](#puppet-server).

**References:** ∅

### Firewall

**Summary:** Custom Puppet module built on top of a 3rd-party module.

**Stakeholders:** TPA

**Action:** [Migrate][]

- Migrate both codebases to puppetized nftables

**Complexity:** [High][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:**

- [puppet-tirewall](https://gitlab.tails.boum.org/tails/puppet-tirewall)
- [puppetlabs-firewall](https://github.com/puppetlabs/puppetlabs-firewall)

### git-annex

**Summary:** Currently used as data backend for
https://torbrowser-archive.tails.net and https://iso-history.tails.net, blocker
for [Gitolite](#gitolite) retirement.

**Stakeholders:** Tails Team

**Action:** [Migrate][] to GitLab's [Git LFS][]

**Complexity:** [Low][]

**Constraints:**

- Blocks the retirement of [Gitolite](#gitolite)

**References:**

- [Git LFS][]

 [Git LFS]: https://docs.gitlab.com/ee/topics/git/lfs/index.html

### Gitolite

**Summary**: Provides repositories used by the Tails Team for development and
release management, as well as data sources for the website.

**Stakeholders:** TPA, Tails Team

**Action:** [Migrate][] to GitLab

- `etcher-binary`: Obsolete (already migrated to GitLab)
- `gitlab-migration-private`: Migrate to GitLab and archive
- `gitolite-admin`: Obsolete (after migration of other repos)
- `isos`: Migrate to GitLab and Git LFS
- `jenkins-jobs`: Migrate to GitLab (note: has hooks)
- `jenkins-lizard-config`: Obsolete
- `mirror-pool-dispatcher`: Obsolete
- `myprivatekeyispublic/testing`: Obsolete
- `promotion-material`: Obsolete (already migrated to GitLab)
- `tails`: Migrate to GitLab (note: has hooks)
- `test`: Obsolete
- `torbrowser-archive`: Migrate to GitLab and Git LFS
- `weblate-gatekeeper`: Migrate to GitLab (note: has hooks)

**Complexity:** [Medium][]

**Constraints:**

- Blocked by the migration of [git-annex](#git-annex)

**References:**

- tpo/tpa/team#41837

### Jenkins

**Summary:** One Jenkins Orchestrator and 12 Jenkins Agents.

**Stakeholders:** Tails Team

**Action:** [Migrate][] to GitLab CI

**Complexity:** [High][]

**Constraints:**

- Blocks the retirement of [VPN](#vpn)

**References:**

- [Tails Issue 18999]: https://gitlab.tails.boum.org/tails/tails/-/issues/18999

### Mirror pool

**Summary:** Tails currently distributes images and updates via volunteer
mirrors that pull from an Rsync server. Selection of the closest mirror is done
using Mirrorbits.

**Stakeholders:** TPA

**Action:** [Migrate][] to [Tor's CDN][]:

- Advantages:
  - Can help mitigate [certain risks](https://gitlab.torproject.org/tpo/tpa/tails/sysadmin/-/issues/18117)
  - Improves the release management process if devs can push to the mirrors (as opposed to wait for 3rd-party mirrors to sync)
- Disadvantages:
  - Bandwidth costs
  - Less global coverage
  - Less volunteer participation

**Complexity:** [Medium][]

**Constraints:** ∅

**References:**

- https://tails.net/contribute/design/mirrors/
- https://gitlab.torproject.org/tpo/tpa/tails/sysadmin/-/issues/18117 
- [Tor's CDN][]

 [Tor's CDN]:  https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/cdn

### Monitoring

**Summary:** Icinga2 and Icingaweb2.

**Stakeholders:** TPA

**Action:** [Migrate][] to Prometheus

**Complexity:** [High][]

**Constraints:**

- Blocked by the merge of [Puppet Server](#puppet-server).

**References:**

- [Icinga2][]

 [Icinga2]: https://tails.net/contribute/working_together/roles/sysadmins/services/#index8h1

### TLS

**Summary:** Let's Encrypt managed by Puppet.

**Stakeholders:** TPA

**Action:** [Migrate][] to Tor's implementation

**Complexity:** [Medium][]

**Constraints:**

- Blocks the migration of [Monitoring](#monitoring)
- Blocked by the merge of [Puppet Server](#puppet-server).

**References:** 

- [`tails::profile::letsencrypt`](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/letsencrypt.pp)

### XMPP bot

**Summary:** It's only feature is to paste URLs and titles on issue mentions.

**Stakeholders:** Tails Team

**Action:** [Migrate][] to the same bot used by TPA

**Complexity:** [Low][]

**Constraints:**

- Blocked by the migration of [XMPP](#xmpp)

**References:**

- Kind of broken because of [this upstream bug](https://github.com/errbotio/errbot/issues/1680).

### XMPP

**Summary:** Dev and support channels in Disroot XMPP server.

**Stakeholders:** Tails Team

**Action:** [Migrate][] to IRC

**Complexity:** [Medium][]

**Constraints:**

- Blocks the migration of [XMPP bot](#xmpp-bot)

**References:** ∅

### Virtualization

**Summary:** Libvirt config is managed by Puppet, VM definitions not, custom deploy script.

**Stakeholders:** TPA

**Action:** [Keep][], as legacy

**Complexity:** [Low][]

- Treat Tails' VMs as legacy and do not create new ones.
- New hosts and VMs will be created in Ganeti.
- If/when hosts become empty, consider whether to retire them or make them part of Ganeti clusters

**Constraints:**

- Blocked by the migration of [Jenkins](#jenkins)
- Blocked by the merge of [Puppet Server](#puppet-server).

**References:** ∅

## Retire

### Bitcoin

**Summary:** Tails' Bitcoin wallet.

**Stakeholders:** Finances

**Action:** [Retire][], hand-over to Tor accounting

**Complexity:** [Low][]

**Constraints:** ∅

**References:**

- [Bitcoin][]

 [Bitcoin]: https://tails.net/contribute/working_together/roles/sysadmins/services/#index2h1

### Tor Bridge

**Summary:** Not used for dev, but rather to "give back to the community".

**Stakeholders:** Tor Users

**Action:** [Retire][]

**Complexity:** [Low][]

**Constraints:** ∅

**References:**

- [Tor Bridge][]

 [Tor Bridge]: https://tails.net/contribute/working_together/roles/sysadmins/services/#index16h1

### VPN

**Summary:** Tinc connecting VMs hosted by 3rd-parties and physical servers.

**Stakeholders:** TPA

**Action:** [Retire][]

- Depending on timeline, could be replaced by Wireguard mesh (if Tor decides to implement it)

**Complexity:** [High][]

**Constraints:**

- Blocked by the migration of [Jenkins](#jenkins)

**References:**

- [Tinc][]

 [Tinc]: https://tails.net/contribute/working_together/roles/sysadmins/services/#index17h1

## Dependency graph

```mermaid
flowchart TD
    classDef keep fill:#9f9,stroke:#090,color:black;
    classDef merge fill:#adf,stroke:#00f,color:black;
    classDef migrate fill:#f99,stroke:#f00,color:black;
    classDef white fill:#fff,stroke:#000;

    subgraph Captions [Captions]
      Keep; class Keep keep
      Merge; class Merge merge
      Migrate; class Migrate migrate
      Retire; class Retire retire

      Low([Low complexity])
      Medium>Medium complexity]
      High{{High complexity}}
    end

    subgraph Independent [Independent of Puppet]
        Calendar([Calendar]) ~~~
        Documentation([Documentation]) ~~~
        PasswordStore([Password Store]) --> Colocations([Colocations]) & Registrars([Registrars]) ~~~
        Mailman>Mailman lists] ~~~
        GitLab>GitLab] ~~~
        Shifts>Shifts] ~~~
        SecurityPolicy{{Security Policy}}
    end

    subgraph PuppetBlockers [Parallellizable]
        AptRepository>APT repository] ~~~
        LimeSurvey>LimeSurvey] ~~~
        Weblate{{Weblate}} ~~~
        git-annex([git-annex]) -->
        Gitolite([Gitolite]) ~~~
        Jenkins{{Jenkins}} -->
        VPN{{VPN}}
        MTA>MTA] ~~~
        Website>Website] ~~~
        MirrorPool{{Mirror pool}} ~~~
        XMPP>XMPP] -->
        XmppBot([XMPP bot]) ~~~
        Bitcoin([Bitcoin]) ~~~
        TorBridge([Tor Bridge])
    end

    subgraph Puppet [Puppet repo and server]
    direction TB
        TorPuppet7>Upgrade Tor's Puppet Server to Puppet 7] --> PuppetModules & CommitSigning & Eyaml
        PuppetModules>Puppet modules] --> HybridPuppet
        Eyaml([EYAML]) --> HybridPuppet
        CommitSigning>Commit signing] --> HybridPuppet
        HybridPuppet{{Puppet Server}}
    end

    subgraph Basic [Basic system functionality]
        WebServer>Web servers] ~~~
        Authentication{{Authentication}} ~~~
        Backups([Backups]) --> Monitoring{{Monitoring}}
        TLS([TLS]) --> Monitoring ~~~
        DNS{{DNS}} ~~~
        Firewall{{Firewall}}
        Authentication ~~~ TLS
    end

    subgraph ToKeep [Services to keep]
        direction TB;
        HedgeDoc([HedgeDoc]) ~~~
        IsoHistory([ISO history]) ~~~
        TbArchive([Tor Browser archive]) ~~~
        BitTorrent([BitTorrent]) ~~~
        WhisperBack([WhisperBack]) ~~~
        Schleuder([Schleuder]) ~~~
        AptSnapshots{{APT snapshots}}
    end

    subgraph Deferred
        EyamlTrocla>EYAML or Trocla]
    end

    Captions ~~~ Puppet & Independent & PuppetBlockers
    Independent ~~~~~ PuppetCodebase
    Puppet --> ToKeep & Basic --> Deferred
    Deferred --> PuppetCodebase{{Consolidated Puppet codebase}}
    PuppetBlockers ----> PuppetCodebase
    PuppetCodebase --> Virtualization([Virtualization])

    class AptRepository merge
    class AptSnapshots keep
    class Authentication merge
    class Backups migrate
    class BitTorrent keep
    class Bitcoin retire
    class Calendar migrate
    class Colocations keep
    class CommitSigning keep
    class DNS migrate
    class DNS migrate
    class Documentation merge
    class Eyaml keep
    class EyamlTrocla migrate
    class Firewall migrate
    class GitLab merge
    class Gitolite migrate
    class HedgeDoc keep
    class HybridPuppet merge
    class IsoHistory keep
    class Jenkins migrate
    class LimeSurvey merge
    class MTA merge
    class Mailman merge
    class MirrorPool migrate
    class Monitoring migrate
    class PasswordStore merge
    class PuppetCodebase merge
    class PuppetModules merge
    class Registrars keep
    class Schleuder keep
    class SecurityPolicy merge
    class Shifts merge
    class TLS migrate
    class TbArchive keep
    class TorBridge retire
    class TorPuppet7 keep
    class VPN retire
    class Virtualization keep
    class WebServer merge
    class Weblate merge
    class Website merge
    class WhisperBack keep
    class XMPP migrate
    class XmppBot migrate
    class git-annex migrate
```

# Timeline

## 2024

  - [Documentation](#documentation) (merge)
  - ~~[Calendar](#calendar)~~ (migration already done)
  - [Colocations](#colocations) (keep)
  - [Registrars](#registrars) (keep)
  - [Password store](#password-store) (merge)
  - [Security Policy](#security-policy) (merge)
  - [Tor bridge](#tor-bridge) (retire)

## 2025

  - [Shifts](#shifts) (merge)
  - Puppet repos and server:
    - [Upgrade Tor's Puppet Server to Puppet 7](tpo/tpa/team#41819)
    - Upgrade and converge Puppet modules
    - Implement commit signing
    - [EYAML](#eyaml) (keep)
    - [Puppet server](#puppet-server) (merge)
  - A plan for [Authentication](#authentication)
  - [Bitcoin](#bitcoin) (retire)
  - [LimeSuvey](#limesurvey) (merge)
  - [Monitoring](#monitoring) (migrate)
  - [Website](#website) (merge)

## 2026

  - Basic system functionality:
    - [Authentication](#authentication) (merge)
    - [Backups](#backups) (migrate)
    - [DNS](#dns) (migrate)
    - [Firewall](#firewall) (migrate)
    - [TLS](#tls) (migrate)
    - [Web servers](#web-servers) (merge)
  - [Mailman](#mailman) (merge)
  - [XMPP](#xmpp) / [XMPP bot](#xmpp-bot) (migrate)

## 2027

  - [APT repository](#apt-repository) (keep)
  - [APT snapshots](#apt-snapshots) (keep)
  - [BitTorrent](#bittorrent) (keep)
  - [HedgeDoc](#hedgedoc) (keep)
  - [ISO history](#iso-history) (keep)
  - [MTA](#mta) (merge)
  - [Mirror pool](#mirror-pool) (migrate)
  - [Schleuder](#schleuder) (keep)
  - [Tor browser archive](#tor-browser-archive) (keep)
  - [Whisperback](#whisperback) (keep)
  - [GitLab](#gitlab) (merge)
  - [git-annex](#git-annex) / [Gitolite](#gitolite) (migrate)

## 2028

  - Decide between [EYAML](#eyaml) or Trocla or both
  - [Weblate](#weblate)

## 2029

  - [Jenkins](#jenkins)
  - [VPN](#vpn)
  - [Virtualization](#virtualization)

# Alternatives considered

## Converge both codebases before merging repositories and Puppet Servers

  This approach would have the following disadvantages:
  - keeping two different Puppet codebase repositories in sync is more prone to
    errors and regressions,
  - no possibility of using exported resources would make some migrations more
    difficult (eg. Backups, Monitoring, TLS, etc)

# References

See the [TPA/Tails sysadmins overview][] document that was used to inform the
decision about the merger.

 [TPA/Tails sysadmins overview]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/tails-merge
