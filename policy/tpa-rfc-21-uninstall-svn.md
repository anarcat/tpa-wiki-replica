---
title: TPA-RFC-21: Uninstall SVN
deadline: 2022-04-18
status: obsolete
---

[[_TOC_]]

Summary: remove the Subversion package on all servers but Gayi.

# Background

Today Debian released a new version of the 'subversion' package with new 
security updates, and I noticed its installed on all our hosts.

# Proposal

Does anyone object to only having it installed by default on gayi.tpo, 
which is our one (hopefully soon-to-be decommissioned) subversion server?

# References

See also the [TPA-RFC-11: SVN retirement proposal](policy/tpa-rfc-11-svn-retirement).
