---
title: TPA-RFC-50: private GitLab pages
deadline: 2023-02-16
status: standard
discussion: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41079
---

Summary: allow GitLab users to publish private GitLab pages

# Background

In our GitLab instance, all GitLab pages are public, that is sites
published by GitLab CI outside of the static-component system have no
access control whatsoever.

GitLab pages does support enabling authentication to hide pages under
GitLab authentication. This was not enabled in our instance.

# Proposal

Enable the GitLab access control mechanisms under the `read_api`
scope.

Note that this might make your GitLab pages inaccessible if your
project was configured to hide them. If that's not something you want,
head to `Settings` -> `General` -> `Visibility` -> `Pages` and make
them public.

# Deadline

This was implemented on 2023-02-16, and this proposal was written to
retroactively inform people of the change.
