---
title: TPA-RFC-3: tools
costs: TODO
approval: TPA?
affected users: TODO
deadline: TODO
status: draft
discussion: TODO
---

Summary: we try to restrict the number of tools users and sysadmins
need to learn to operate in our environment. This policy documents
which tools we use.

# Background

A proliferation of tools can easily creep up into an organisation. By
limiting the number of tools in use, we can keep training and
documentation to a more reasonable size. There's also the off chance
that someone might already know *all* or a large proportion the tools
currently in use if the set is smaller and standard.

# Proposal

This proposal formally defines which tools are used and offered by TPA
for various things inside of TPO.

We try to have one and only one tool for certain services, but
sometimes we have many. In that case, we try to deprecate one of the
tools in favor of the other.

## Scope

This applies to services provided by TPA, but not necessarily to all
services available inside TPO. Service admins, for example, might make
different decisions than the ones described here for practical
reasons.

## Tools list

This list consists of the known policies we currently have
established.

 1. version control: git, gitolite
 2. operating system: Debian packages (official, backports,
    third-party and TPA)
 3. host installation: debootstrap, FAI
 4. ad-hoc tools: SSH, Cumin
 5. directory servers: OpenLDAP, BIND, ud-ldap, Hiera
 6. authentication servers: OpenLDAP, ud-ldap
 7. time synchronisation: NTP (`ntp` Debian package, from ntp.org)
 8. Network File Servers: DRBD
 9. File Replication Servers: static mirror system
 10. Client File Access: N/A
 12. Client OS Update: unattended-upgrades, needrestart, dsa nagios
     checks, multiple reboot tools ([ticket #33406](https://bugs.torproject.org/33406))
 13. Client Configuration Management: Puppet
 14. Client Application Management: Debian packages, systemd
     lingering, cron `@reboot` targets (deprecated)
 15. Mail: SMTP/Postfix, Mailman, ud-ldap, dovecot (on gitlab)
 16. Printing: N/A
 17. Monitoring: syslog-ng central host, Nagios, Prometheus, Grafana,
     no paging
 18. password management: pwstore
 19. help desk: Trac, email, IRC
 20. backup services: bacula, postgresql hot sync
 21. web services: Apache, Nginx, Varnish (deprecated), haproxy
     (deprecated)
 22. documentation: ikiwiki, Trac wiki
 23. datacenters: Hetzner cloud, Hetzner robot, Cymru, Sunet, Linaro,
     Scaleway (deprecated)
 24. Programming languages: Python, Perl (deprecated), shell (for
     short programs), also in use at Tor: Ruby, Java, Golang, Rust, C,
     PHP, Haskell, Puppet, Ansible, YAML, JSON, XML, CSV

## TODO

 1. figure out scope... list has grown big already
 2. are server specs part of this list?
 3. software raid?
 4. add Gitlab issues to help desk, deprecate Trac
 5. add Fabric to host installs and ad-hoc tools
 6. consider Gitlab wiki as a ikiwiki replacement?
 7. add RT to help desk?

## Examples

 * all changes to servers should be performed through Puppet, as much
   as possible...
 * ... except for services not managed by TPA ("service admin stuff"),
   which can be deployed by hand, Ansible, or any other tool
 * 

# References

Drafting this policy was inspired by the [limiting tool dev
choices](https://utcc.utoronto.ca/~cks/space/blog/sysadmin/LimitingToolDevChoices) blog post from [Chris Siebenmann](https://utcc.utoronto.ca/~cks/) from the [University
of Toronto Computer Science department](https://www.cs.toronto.edu/).

The tool classification is a variation of the [infastructures.org](http://www.infrastructures.org/)
[checklist](http://www.infrastructures.org/bootstrap/checklist.shtml), with item 2 changed from "Gold Server" to "Operating
System". The naming change is rather dubious, but I felt that "Gold
Server" didn't really apply anymore in the context of configuration
management tools like Puppet (which is documented in item 13). Debian
is a fundamental tool at Tor and it feels critical to put it first and
ahead of everything else, because it's one thing that we rely on
heavily. It also *does* somewhat acts as a "Gold Server" in that it's
a static repository of binary code. We also do not have uniform
"Client file access" (item 10) and "Printing" (item 16). Item 18
("Password management") was also added.
