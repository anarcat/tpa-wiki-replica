---
title: TPA-RFC-37: Lektor replacement
costs: TODO
approval: TPA
affected users: TODO
deadline: TODO
status: draft
discussion: https://gitlab.torproject.org/tpo/web/team/-/issues/42
---

[[_TOC_]]

Summary: TODO

# Background

Lektor is the static site generator (SSG) that is used across almost
all sites hosted by the Tor Project. We are having repeated serious
issues with Lektor, to a point where it is pertinent to evaluate
whether it would be easier to convert to another SSG rather than try
to fix those issues.

# Requirements

TODO: set requirements, clearly state bugs to fix

## Must have

## Nice to have

## Non-Goals

# Personas

TODO: write a set of personas and how they are affected by the current
platform

# Alternatives considered

TODO: present the known alternatives and a thorough review of them.

# Proposal

TODO: After the above review, propose a change (or status quo).

# References

 * [TPA-RFC-16: Replacing Lektor 18n plugin](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-16-replacing-lektor-i18n-plugin) is related, see also
   [tpo/web/team#28](https://gitlab.torproject.org/tpo/web/team/-/issues/28)
 * [discussion ticket](https://gitlab.torproject.org/tpo/web/team/-/issues/42)
