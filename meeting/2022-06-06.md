# Roll call: who's there and emergencies

Anarcat, Kez, and Lavamind present.

No emergencies.

# Roadmap / OKR review

Only one month left to the quarter! Where are we? As a reminder, we
generally hope to accomplish 60-70% of OKRs, by design, so they're not
supposed to be all done.

[TPA OKRs][]: roughly 17% done

* [mail services][] work has not started, the RFC proposal took longer
  than expected and we're waiting on a decision before starting any work
* [Retirements][] might progress with a gitolite/gitweb retirement RFC
  spearheaded by anarcat
* [codebase cleanup][] work has progressed only a little, often gets
  pushed to the side by emergencies
* [Bullseye upgrades][] has only 6 machines left in the second batch. We
  need to close 3 more tickets to get at 60% on *that* OKR, and that's
  actually likely: the [second batch][] is likely to finish by the end
  of the month, the [primary ganeti cluster upgrade][] is planned, and
  the [PostgreSQL warnings][] will be done today
* [High-performance cluster][]: "New Relic" is giving away money, we
  need to write a grant proposal in 3 days though, possibly not going to
  happen

[TPA OKRs]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2022
[mail services]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/4
[retirements]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/1
[codebase cleanup]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/3
[bullseye upgrades]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/5
[second batch]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40692
[primary ganeti cluster upgrade]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40689
[PostgreSQL warnings]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40776
[High-performance cluster]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/2

[Web OKRs][]: 42% done overall!

* The donate OKR is about 25% complete
* translation OKR seems complete, no one has any TODO items on that
  anyways, so considered done (100%!)
* docs OKR:
  * dev.tpo work hasn't started yet, might be possible to start
    depending on kez availability? 
  * documentation improvement might be good for hack week

[web OKRs]: https://gitlab.torproject.org/tpo/web/team/-/wikis/roadmap/2022

# Holidays

Update on holiday dates, everyone agrees with the plan. Details are
private, see tor-internal emails, and the Nextcloud calendars for the
authoritative dates.

# This week's All-Hands

 * lavamind will talk about the blog

 * if there is still time after, we can open for comments or questions
   about the mail proposal

# Dashboard review

We looked at the global dashboards:

 * https://gitlab.torproject.org/tpo/tpa/team/-/boards/117
 * https://gitlab.torproject.org/groups/tpo/web/-/boards
 * https://gitlab.torproject.org/groups/tpo/tpa/-/boards

... and per-user dashboards, not much to reshuffle.

# Icinga vs Prometheus again

Validate requirements, discuss the alternatives. Requirements weren't
ready, postponed.

# Other discussions

No other discussion came up.

# Next meeting

Next meeting is on a tuesday because of the holiday, we should talk
about OKRs again, and the Icinga vs Prometheus question.

# Metrics of the month

 * hosts in Puppet: 96, LDAP: 96, Prometheus exporters: 160
 * number of Apache servers monitored: 29, hits per second: 299
 * number of self-hosted nameservers: 6, mail servers: 8
 * pending upgrades: 0, reboots: 0
 * average load: 2.65, memory available: 4.32 TiB/5.91 TiB, running
   processes: 933
 * disk free/total: 37.10 TiB/92.61 TiB
 * bytes sent: 411.24 MB/s, received: 289.26 MB/s
 * planned bullseye upgrades completion date: 2022-10-14
 * [GitLab tickets][]: 183 tickets including...
   * open: 0
   * icebox: 151
   * backlog: 14
   * next: 9
   * doing: 5
   * needs review: 1
   * needs information: 3
   * (closed: 2755)

 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

Upgrade prediction graph lives at https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades/bullseye/

We have managed to still speed up our upgrades progression from last
time, moving from December to October as a predicted completion
date. That's not as fast as last estimate (2 years acceleration!) but
it's still quite satisfying.
