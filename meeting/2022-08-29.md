# Roll call: who's there and emergencies

anarcat, kez, lavamind.

# Dashboard review

Normal per-user check-in:

 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=anarcat
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=kez
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=lavamind

General dashboards:

 * https://gitlab.torproject.org/tpo/tpa/team/-/boards/117
 * https://gitlab.torproject.org/groups/tpo/web/-/boards
 * https://gitlab.torproject.org/groups/tpo/tpa/-/boards

We have decided we transfer the OKRs from a bi-quaterly roadmap to a
yearly objective. It seems realistic that we manage to accomplish a
significant part of the OKRs by the end of the year, even if only a
plan for retirements, mail migration, and finish almost all the
bullseye upgrades.

# TPA-RFC-33: monitoring requirements adoption

Still one pending MR here to review / discuss, postponed.

# Ireland meeting

Review the sessions anarcat proposed. There's a concern about one of
the team members not being able to attend. We discussed how we have
some flexibility on scheduling so that some sessions arrive at the
right time and how we could stream sessions.

# Next meeting

Next meeting should be in early October.

# Metrics of the month

 * hosts in Puppet: 96, LDAP: 96, Prometheus exporters: 164
 * number of Apache servers monitored: 29, hits per second: 468
 * number of self-hosted nameservers: 6, mail servers: 10
 * pending upgrades: 17, reboots: 0
 * average load: 0.72, memory available: 4.74 TiB/5.87 TiB, running
   processes: 793
 * disk free/total: 31.06 TiB/91.86 TiB
 * bytes sent: 396.67 MB/s, received: 268.32 MB/s
 * planned bullseye upgrades completion date: 2022-10-02
 * [GitLab tickets][]: 180 tickets including...
   * open: 0
   * icebox: 144
   * backlog: 17
   * next: 11
   * doing: 4
   * needs information: 3
   * needs review: 1
   * (closed: 2847)

 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

Upgrade prediction graph lives at https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades/bullseye/
