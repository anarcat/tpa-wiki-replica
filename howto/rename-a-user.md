[[_TOC_]]

This document explains how to handle requests to rename a user account.

# Requirements

- the new LDAP username
- the new "full name"
- a new or updated GPG key with the new email
- a new mail forwarding address, if needed

# Main procedure

1. Update `account-keyring.git` with the new (or updated) GPG key

2. With `ldapvi`, update the user and group names in the LDAP database
   (including the DN), along with the new GPG fingerprint if a new key is to be
   associated with the account and forwarding address if applicable

3. Using `cumin`, rename home directories on hosts

4. Optionally, add the previous forwarding to the virtual alias map on `eugeni`

5. Update the information on the main website

# GitLab

GitLab users may rename their own accounts with the User Settings panel.

# Nextcloud

Changing the login name is [not supported][] at all in Nextcloud, only the
display name can be changed.

[not supported]: https://docs.nextcloud.com/server/latest/admin_manual/configuration_user/user_configuration.html#renaming-a-user

If a new account is created as part or the renaming process, it's possible to
"transfer" files and shares from one account to the other using the
[files:transfer-ownership][] command via the CLI. This particular option is
however untested, and TPA doesn't have access to the hosted Nextcloud CLI.

[files:transfer-ownership]: https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#transfer

# Other

It's a good idea to grep the `tor-puppet.git` repository, this can catch
instances of the old username existing in places like `/etc/subuid`.
