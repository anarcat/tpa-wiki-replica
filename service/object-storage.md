[Object Storage](https://en.wikipedia.org/wiki/Object_storage) actually designates a variety of data storage
mechanisms. In our case, we actually refer to the ad-hoc standard
developed under the [Amazon S3](https://en.wikipedia.org/wiki/Amazon_S3) umbrella.

This page particularly documents the [MinIO][] server
(`minio.torproject.org`, currently a single-server
`minio-01.torproject.org`) managed by TPA, mainly for [GitLab](howto/gitlab)'s
Docker registry, but it could eventually be used for other purposes.

[[_TOC_]]

# Tutorial

<!-- simple, brainless step-by-step instructions requiring little or -->
<!-- no technical background -->

# How-to

## Access the admin interface

To see if the service works, you can connect to the admin interface
through <https://localhost:9090> with a normal web browser. Because of
limitations in containers port forwarding, this is currently only
available through `localhost`, which means you need to do a port
forward over SSH to access it first:

    ssh -L 9090:localhost:9090 minio-01.torproject.org

Web browsers will yield a certification name mismatch warning which
can be safely ignored. See [Security and risk assessment](#security-and-risk-assessment) for a
discussion on why that is setup that way.

The username is `admin` and the password is in `/etc/default/minio` on
the server (currently `minio-01`). You should use that account only to
create or manage other, normal user accounts with lesser access
policies. See [authentication](#authentication) for details.

## Configure the local mc client

You *must* use the web interface (above) to create a first access
key for the admin user.

Then record the access key on the UNIX `root` account with:

    podman run --network=host -v $HOME/.mc:$HOME/.mc --rm -it quay.io/minio/mc alias set admin https://minio-01.torproject.org:9000

Notice how we currently use container images to run the `mc` tool. The
boilerplate can be reduced with something like:

    alias mc="podman run --network=host -v $HOME/.mc:$HOME/.mc --rm -it quay.io/minio/mc"

In which case the previous command can be done with the simpler:

    mc alias set admin https://minio-01.torproject.org:9000

Further examples below will use the alias.

The above configuration will make further commands possible, see for
example [creating a bucket](#create-a-bucket).

Note that an access key is already pre-configured on the `minio-01`
server, in the root account, so most commands below are better ran
there.

## Create a user

To create a new user, you can use the `mc` client configured
above. Here, for example, we create a `gitlab` user:

    mc admin user add admin/gitlab

By default, a user has no privileges. You can grant it access by
attaching a policy, see below.

Typically, however, you might want to create an access key
instead. For example, if you are creating a new bucket for some GitLab
service, you would create an access key under the `gitlab` account
instead of an entirely new user account.

## Define and grant an access policy

The [default policies](https://min.io/docs/minio/container/administration/identity-access-management/policy-based-access-control.html#built-in-policies) are quite broad and give access to *all*
buckets on the server, which is almost as the admin user except for
the [admin:* namespace](https://min.io/docs/minio/container/administration/identity-access-management/policy-based-access-control.html#policy-action.admin). So we need to make a bucket policy. First
create a file with this JSON content:

```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
            "s3:*"
        ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::gitlab/*", "arn:aws:s3:::gitlab"
      ],
      "Sid": "BucketAccessForUser"
    }
  ]
}
```

This was inspired by  [Jai Shri Ram's MinIO Bucket Policy Notes](https://blog.nikhilbhardwaj.in/2020/02/25/minio-bucket-policy/),
but we actually grant all `s3:*` privileges on the given `gitlab`
bucket and its contents:

 - `arn:aws:s3:::gitlab` grants bucket operations access, such as creating
    the bucket or listing all its contents

 - `arn:aws:s3:::gitlab/*` grants permissions on all the bucket's objects

That policy needs to be fed to MinIO using the web interface or `mc`
with:

    mc admin policy create admin gitlab-bucket-policy /root/.mc/gitlab-bucket-policy.json

Then the policy can be attached an existing user with, for example:

    mc admin policy attach admin gitlab-bucket-policy --user=gitlab

### Resource definitions

Here are some extra resource definition examples that may be used:

 - `arn:aws:s3:::gitlab-*` for operations on any bucket with a `gitlab-`
    prefix, including creating such buckets

 - `arn:aws:s3:::gitlab-*/*` for any objects contained in any bucket with the
   `gitlab-` prefix

## Create an access key

To create an access key, you should login the web interface with a
normal user (*not* `admin`, [authentication](#authentication) for details) and
create a key in the "Access Keys" tab.

An access key can be created for another user (below `gitlab`) on the
commandline with:

    mc admin user svcacct add admin gitlab

This will display the credentials in plain text on the terminal, so
watch out for shoulder surfing.

The key will inherit the policies established above for the user. So
unless you want the access key have the same access as the user, make
sure to attach a policy to the access key.

If you have just created a user, you might want to add an alias for
that user on the server as well, so that future operations can be done
through that user instead of `admin`, for example:

    mc alias set gitlab http://minio-01.torproject.org:9000

## Create a bucket

A bucket can be created on a MinIO server using the `mc` commandline
tool.

WARNING: you should NOT create buckets under the main `admin`
account. Create a new account for your application as `admin`, then as
that new account, create a specific access key, as per above.

The following will create a bucket named `foo` under the main `admin`
account:

    root@minio-01:~# mc mb admin/foo
    Bucket created successfully `foo`.

Try creating the same bucket again, to confirm it really exists, it
should fail like this:

    root@minio-01:~# mc mb admin/foo
    mc: <ERROR> Unable to make bucket `local/foo`. Your previous request to create the named bucket succeeded and you already own it.

You should also see the bucket in the web interface.

Here's another example, where we create a `gitlab-registry` bucket
under the `gitlab` account:

    mc mb gitlab/gitlab-registry

## Use rclone as an object storage client

The incredible [rclone][] tool can talk to object storage and might be
the easiest tools to do manual changes to buckets and object storage
remotes in general.

First, You'll need an access key (see above) to configure the
remote. This can be done interactively with:

    rclone config

Or directly on the commandline with something like:

    rclone config create minio s3 provider Minio endpoint https://minio.torproject.org:9000/ access_key_id test secret_access_key [REDACTED]

From there you can do a bunch of things. For example, list existing
buckets with:

    rclone lsd minio:

Copying a file in a bucket:

    rclone copy /etc/motd minio:gitlab

The file should show up in:

    rclone ls minio:gitlab

See also the [rclone s3 documentation](https://rclone.org/s3/) for details.

[rclone s3 documentation]: https://rclone.org/s3/

## Password resets

MinIO is primarily access through access tokens, issued to users. To
create a new access token, you need a user account.

If that password is lost, you should follow one of two procedures,
depending on whether you need access to the main administrator account
(`admin`, which is the one who can grant access to other accounts) or
a normal user account.

### Normal user

To reset the password on a normal user, you must login through the web
interface; it doesn't seem possible to reset the password on a normal
user through the `mc` command.

### Admin user

The admin user password is set in `/etc/default/minio`. It can be
changed by following a part of the [installation instructions](#installation),
namely:

    PASSWORD=$(tr -dc '[:alnum:]' < /dev/urandom | head -c 32)
    echo "MINIO_ROOT_PASSWORD=$PASSWORD" > /etc/default/minio
    chmod 600 /etc/default/minio

... and then restarting the service:

    systemctl restart container-minio.service

### Access keys

Access keys secrets cannot be reset: the key must be deleted and a new
one must be created in its place.

A better way to do this is to create a *new* key and mark the old one
as expiring. To rotate the GitLab secrets, for example, a new key
named `gitlab-registry-24` was created (`24` being the year, but it
could be anything), and the `gitlab-registry` key was marked as
expiring 24h after. The new key was stored in Trocla and the key name,
in Puppet.

The runner cache token is more problematic, as the Puppet module
doesn't update it automatically once the runner is registered. That
needs to be modified by hand.

## Pager playbook

### Restarting the service

The MinIO service runs under the `container-minio.service` unit. To
restart it if it crashed, simply run:

    systemctl restart container-minio.service

## Disaster recovery

If the server is lost with all data, a new server should be rebuilt
(see [installation](#installation) and a recovery from backups should be
attempted.

See also the [upstream Recovery after Hardware Failure](https://min.io/docs/minio/linux/operations/data-recovery.html)
documentation.

# Reference

## Installation

We followed the [hardware checklist](https://min.io/docs/minio/linux/operations/checklists/hardware.html#minio-hardware-checklist) to estimate the memory
requirement which happily happened to match the default `8g` parameter
in our Ganeti VM installation instructions. We also set 2 vCPUs but
that might need to change.

We setup the server with a `plain` backend to save disk on the nodes,
with the understanding this service has lower availability
requirements than other services. It's especially relevant since, if
we want higher availability, we'll setup multiple nodes, so
network-level RAID is redundant here.

The actual command used to create the VM was:

    gnt-instance add \
      -o debootstrap+bookworm \
      -t plain --no-wait-for-sync \
      --net 0:ip=pool,network=gnt-dal-01 \
      --no-ip-check \
      --no-name-check \
      --disk 0:size=10G \
      --disk 1:size=1000G \
      --backend-parameters memory=8g,vcpus=2 \
      minio-01.torproject.org

We assume the above scheme is compatible with the [Sequential
Hostnames](https://min.io/docs/minio/linux/operations/install-deploy-manage/deploy-minio-multi-node-multi-drive.html#sequential-hostnames) requirements in the MinIO documentation. They use
`minio{1...4}.example.com` but we assume the `minio` prefix is
user-chosen, in our case `minio-0`.

The `profile::minio` class must be included in the role (currently
`role::object_storage`) for the affected server. It configures the
firewall, podman, and sets up the systemd service supervising the
container.

Once the install is completed, you should have the admin password in
`/etc/default/minio`, which can be used to [access the admin
interface](#access-the-admin-interface) and, from there, pretty much do everything you need.

### Region configuration

Some manual configuration was done after installation, namely setting
access tokens, configuring buckets and the region. The latter is done
with:

    mc admin config set admin/ region name=dallas

Example:

    root@minio-01:~# mc admin config set admin/ region name=dallas
    Successfully applied new settings.
    Please restart your server 'mc admin service restart admin/'.
    root@minio-01:~# systemctl restart container-minio.service
    root@minio-01:~# mc admin config get admin/ region
    region name=dallas

### Manual installation

Those are notes taken during the original installation. That was later
converted with Puppet, in the aforementioned `profile::minio` class,
so you shouldn't need to follow this to setup a new host, Puppet
should set up everything correctly.

The [quickstart guide][] is easy enough to follow to get us started,
but we do some tweaks to:

 * make the `podman` commandline more self-explanatory using long
 options

 * assign a name to the container

 * use `/srv` instead of `~`

 * explicitly generate a (strong) password, store it in a config file,
   and use that

 * just create the container (and not start it), delegating the
   container management to systemd instead, as per [this guide](https://www.redhat.com/sysadmin/improved-systemd-podman)

This is the actual command we use to *create* (not start!) the
container:

    PASSWORD=$(tr -dc '[:alnum:]' < /dev/urandom | head -c 32)
    echo "MINIO_ROOT_PASSWORD=$PASSWORD" > /etc/default/minio
    chmod 600 /etc/default/minio
    mkdir -p /srv/data

    podman create \
       --name minio \
       --publish 9000:9000 \
       --publish 9090:9090 \
       --volume /srv/data:/data \
       --env "MINIO_ROOT_USER=admin" \
       --env "MINIO_ROOT_PASSWORD" \
       quay.io/minio/minio server /data --console-address ":9090"

We store the password in a file because it will be used in a systemd
unit.

This is how the systemd unit was generated:

    podman generate systemd --new --name minio | sed 's,Environment,EnvironmentFile=/etc/default/minio\nEnvironment,' > /etc/systemd/system/container-minio.service

Then the unit was enabled and started with:

    systemctl enable container-minio.service && systemctl start container-minio.service

That starts MinIO with an admin interface on <https://localhost:9090>
and the API on <https://localhost:9000>, even though the console
messages mention addresses in the `10.0.0.0/8` network.

You can use the web interface to create the buckets, or the [mc
client][] which is [also available as a Docker container][].

[quickstart guide]: https://min.io/docs/minio/container/index.html
[mc client]: https://min.io/docs/minio/linux/reference/minio-mc.html
[also available as a Docker container]: https://quay.io/repository/minio/mc

The installation was done in [issue tpo/tpa/team#41257](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41257) which may
have more details.

The actual systemd configuration was modified since then to adapt to
various constraints, for example the TLS configuration, container
updates, etc.

We could consider Podman's [quadlets](https://www.redhat.com/sysadmin/quadlet-podman), but those shipped only in
Podman 4.4, which barely missed the bookworm release. To reconsider in
Debian Trixie.

## Upgrades

Upgrades are handled automatically through the [built-in podman
self-updater](https://www.redhat.com/sysadmin/improved-systemd-podman), [podman-auto-update](https://github.com/containers/podman/blob/v2.0/docs/source/markdown/podman-auto-update.1.md). The way this works is the
container is ran with `--pull=never` so that a new image is *not*
pulled when the container is started.

Instead, the container is labeled with
`io.containers.autoupdate=image` and *that* is what makes `podman
auto-update` pull the new image.

The job is scheduled by the `podman` package under systemd, you can
see the current status with:

    systemctl status podman-auto-update

Here are the [full logs](https://serverfault.com/a/974304) of an example successful run:

    root@minio-01:~# journalctl _SYSTEMD_INVOCATION_ID=`systemctl show -p InvocationID --value podman-auto-update.service` --no-pager
    Jul 18 19:28:34 minio-01 podman[14249]: 2023-07-18 19:28:34.331983875 +0000 UTC m=+0.045840045 system auto-update
    Jul 18 19:28:35 minio-01 podman[14249]: Trying to pull quay.io/minio/minio:latest...
    Jul 18 19:28:36 minio-01 podman[14249]: Getting image source signatures
    Jul 18 19:28:36 minio-01 podman[14249]: Copying blob sha256:27aad82ab931fe95b668eac92b551d9f3a1de15791e056ca04fbcc068f031a8d
    Jul 18 19:28:36 minio-01 podman[14249]: Copying blob sha256:e87e7e738a3f9a5e31df97ce1f0497ce456f1f30058b166e38918347ccaa9923
    Jul 18 19:28:36 minio-01 podman[14249]: Copying blob sha256:5329d7039f252afc1c5d69521ef7e674f71c36b50db99b369cbb52aa9e0a6782
    Jul 18 19:28:36 minio-01 podman[14249]: Copying blob sha256:7cdde02446ff3018f714f13dbc80ed6c9aae6db26cea8a58d6b07a3e2df34002
    Jul 18 19:28:36 minio-01 podman[14249]: Copying blob sha256:5d3da23bea110fa330a722bd368edc7817365bbde000a47624d65efcd4fcedeb
    Jul 18 19:28:36 minio-01 podman[14249]: Copying blob sha256:ea83c9479de968f8e8b5ec5aa98fac9505b44bd0e0de09e16afcadcb9134ceaa
    Jul 18 19:28:39 minio-01 podman[14249]: Copying config sha256:819632f747767a177b7f4e325c79c628ddb0ca62981a1a065196c7053a093acc
    Jul 18 19:28:39 minio-01 podman[14249]: Writing manifest to image destination
    Jul 18 19:28:39 minio-01 podman[14249]: Storing signatures
    Jul 18 19:28:39 minio-01 podman[14249]: 2023-07-18 19:28:35.21413655 +0000 UTC m=+0.927992710 image pull  quay.io/minio/minio
    Jul 18 19:28:40 minio-01 podman[14249]:             UNIT                     CONTAINER             IMAGE                POLICY      UPDATED
    Jul 18 19:28:40 minio-01 podman[14249]:             container-minio.service  0488afe53691 (minio)  quay.io/minio/minio  registry    true
    Jul 18 19:28:40 minio-01 podman[14385]: 09b7752e26c27cbeccf9f4e9c3bb7bfc91fa1d2fc5c59bfdc27105201f533545
    Jul 18 19:28:40 minio-01 podman[14385]: 2023-07-18 19:28:40.139833093 +0000 UTC m=+0.034459855 image remove 09b7752e26c27cbeccf9f4e9c3bb7bfc91fa1d2fc5c59bfdc27105201f533545

You can also see when the next job will run with:

    systemctl status podman-auto-update.timer

## SLA

This service is *not* provided in high availability mode, which was
deemed too complex for a first prototype in [TPA-RFC-56][],
particularly using MinIO with a containers runtime.

Backups, in particular, are not guaranteed to be functional, see
[backups](#backups) for details.

## Design and architecture

The design of this service was discussed in [tpo/tpa/team#40478][] and
proposed in [TPA-RFC-56][]. It is currently a single virtual machine
in the gnt-dal cluster running MinIO, without any backups or
redundancy.

This is assumed to be okay because the data stored on the object
storage is considered disposable, as it can be rebuilt. For example,
the first service which will use the object storage, GitLab Registry,
generates artifacts which can normally be rebuilt from scratch without
problems.

If the service becomes more popular and is more heavily used, we might
setup a more highly available system, but at that stage we'll need to
look again more seriously at alternatives from [TPA-RFC-56][] since
MinIO's distributed are much more complicated and hard to manage than
their competitors. Garage and Ceph are the more likely alternatives,
in that case.

We do not use the advanced distributed capabilities of MinIO, but
those are documented in [this upstream architecture page](https://min.io/docs/minio/linux/operations/concepts/architecture.html) and [this
design document](https://github.com/minio/minio/blob/master/docs/distributed/DESIGN.md).

[TPA-RFC-56]: policy/tpa-rfc-56-large-file-storage
[tpo/tpa/team#40478]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40478

## Services

The MinIO daemon runs under `podman` and `systemd` under the
`container-minio.service` unit.

## Storage

In a single node setup, files are stored directly on the local disk,
but with extra metadata mangled with the file content. For example,
assuming you have a directory setup like this:

    mkdir test
    cd test
    touch empty
    printf foo > foo

... and you copy that directory over to a MinIO server:

    rclone copy test minio:test-bucket/test

On the MinIO server's data directory, you will find:

    ./test-bucket/test
    ./test-bucket/test/foo
    ./test-bucket/test/foo/xl.meta
    ./test-bucket/test/empty
    ./test-bucket/test/empty/xl.meta

The data is stored in the `xl.meta` files, and is stored as binary
with a bunch of metadata prefixing the actual data:

    root@minio-01:/srv/data# strings gitlab/test/empty/xl.meta | tail
    x-minio-internal-inline-data
    true
    MetaUsr
    etag
     d41d8cd98f00b204e9800998ecf8427e
    content-type
    application/octet-stream
    X-Amz-Meta-Mtime
    1689172774.182830192
    null
    root@minio-01:/srv/data# strings gitlab/test/foo/xl.meta | tail
    MetaUsr
    etag
     acbd18db4cc2f85cedef654fccc4a4d8
    content-type
    application/octet-stream
    X-Amz-Meta-Mtime
    1689172781.594832894
    null
    StbC
    Efoo

It is *possible* that such data store could be considered consistent
if quiescent, but there's no guarantee about that by MinIO.

There's also a whole `.minio.sys` next to the bucket directories which
contain metadata about the buckets, user policies and configurations,
again using the obscure `xl.meta` storage. This is also assumed to be
hard to backup.

In distributed setups, MinIO uses [erasure coding](https://min.io/docs/minio/linux/operations/concepts/erasure-coding.html#minio-erasure-coding) to distribute
objects across multiple servers. According to their documentation:

> MinIO Erasure Coding is a data redundancy and availability feature
> that allows MinIO deployments to automatically reconstruct objects
> on-the-fly despite the loss of multiple drives or nodes in the
> cluster. Erasure Coding provides object-level healing with
> significantly less overhead than adjacent technologies such as RAID
> or replication.

This implies that the actual files on disk are not readily readable
using normal tools in a distributed setup.

According to [Stack Overflow](https://stackoverflow.com/questions/70425487/how-can-i-access-minio-files-on-the-file-system), there is a proprietary extension to
the `mc` commandline called [mc support inspect](https://min.io/docs/minio/linux/reference/minio-mc/mc-support-inspect.html#command-mc.support.inspect) that allows
inspecting on-disk files, but it requires a "MinIO SUBNET"
registration, whatever that means.

## Queues

MinIO has a built-in [lifecycle management](https://min.io/docs/minio/container/administration/object-management/object-lifecycle-management.html) where object can be
configured to have an expiry date. That is done automatically inside
MinIO with a [low priority object scanner](https://min.io/docs/minio/container/administration/object-management/object-lifecycle-management.html#lifecycle-management-object-scanner).

## Interfaces

There are two main interfaces, the [S3 API](https://en.wikipedia.org/wiki/Amazon_S3) on port `9000` and the
MinIO management console on port `9090`.

The management console is limited to `localhost` and is only
accessible with port forwarding over SSH, see [Accessing the admin
interface](#access-the-admin-interface) for details, and [Security and risk assessment](#security-and-risk-assessment) for a
discussion.

The main S3 API is available globally at
<https://minio.torproject.org:9000>, a CNAME that currently points at
the `minio-01` instance.

Note that this URL, if visited in a web browser, redirects to the
`9090` interface, which is blocked. This can be quite confusing to
users, and we're thinking about how to best improve this without
necessarily opening up the admin interface to the world.

## Authentication

We use the built-in [MinIO identity provider](https://min.io/docs/minio/container/administration/identity-access-management/minio-identity-management.html#minio-internal-idp). Access control is
given to users which are in turn issued access tokens.

The `admin` user is defined in `/etc/default/minio` on `minio-01` and has
an access token saved in `/root/.mc` that can be used with the `mc`
commandline client, see the [tests section](#tests) for details.

The `admin` user MUST only be used to manage other user accounts, as
an access key leakage would be catastrophic. Access keys basically
impersonate a user account, and while it's possible to have access
policies per token, we've made the decision to do access controls with
user accounts instead, as that seemed more straightforward.

The normal user accounts are typically accessed with tokens saved as
aliases on the main `minio-01` server. If that access is lost, you can
use the [password reset](#password-reset) procedures to recover.

Finally, there's a secret token to access the MinIO statistics that's
[generated on the fly](https://min.io/docs/minio/linux/operations/monitoring/collect-minio-metrics-using-prometheus.html). See the [monitoring and metrics section](#monitoring-and-metrics).

The HTTPS certificate is managed by our normal Let's Encrypt
certificate rotation, but required us to pull the `DH PARAMS`, see
[this limitation of crypto/tls in Golang](https://github.com/golang/go/issues/38788) and commit
letsencrypt-domains@ee1a0f7 (stop appending DH PARAMS to certificates
files, 2023-07-11) for details.

## Implementation

MinIO is implemented in Golang, as a single binary.

## Related services

The service is currently used by the [Gitlab service](howto/gitlab).

## Issues

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search] with the
label ~"Object Storage".

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues?label_name%5B%5D=Object Storage

Upstream has an [issue tracker on GitHub](https://github.com/minio/minio/issues/) that is quite clean (22
open issues out of 6628) and active (4 opened, 71 closed issues in the
last month as of 2023-07-12).

MinIO offers a [commercial support service](https://min.io/pricing) which provides 24/7
support with a <48h SLA at 10$/TiB/month. Their [troubleshooting
page](https://min.io/docs/minio/linux/operations/troubleshooting.html) also mentions a [community Slack channel](https://slack.min.io/).

## Maintainer

anarcat setup this service in July 2023 and TPA is responsible for
managing it.

## Users

The service is currently used by the [Gitlab service](howto/gitlab) but may be
expanded to other services upon request.

## Upstream

[MinIO][] is a well-known object storage provider. It is [not packaged
in Debian][]. It has regular releases, but they do not have release
numbers conforming to the semantic versioning standard. Their support
policy is unclear.

[MinIO]: https://min.io/
[not packaged in Debian]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=859207

### Licensing dispute

MinIO are involved in a [licensing dispute][] with commercial storage
providers ([Weka][] and [Nutanix][]) because the latter used MinIO in
their products without giving attribution. See also [this hacker news
discussion][32148007].

It should also be noted that they switched to the AGPL relatively
recently.

This is not seen as a deal-breaker in using MinIO for TPA.

[Weka]: https://www.weka.io/
[Nutanix]: https://www.nutanix.com/
[32148007]: https://news.ycombinator.com/item?id=32148007
[licensing dispute]: https://blocksandfiles.com/2023/03/26/we-object-minio-says-no-more-open-license-for-you-weka/

## Monitoring and metrics

The main [Prometheus](service/prometheus) server is configured to scrape metrics
directly from the `minio-01` server. This was done by running the
following command on the server:

    mc admin prometheus generate admin

... and copying the bearer token into the Prometheus configuration
(`profile::::prometheus::server::internal` in Puppet). Look for
`minio_prometheus_jwt_secret`.

The [upstream monitoring metrics](https://min.io/docs/minio/linux/operations/monitoring/collect-minio-metrics-using-prometheus.html#minio-metrics-collect-using-prometheus) do not mention it, but there's a
[range of Grafana dashboards](https://www.startpage.com/do/metasearch.pl?query=inurl:https://grafana.com/grafana/dashboards minio) as well. Unfortunately, we couldn't
find a working one in [our search](https://www.startpage.com/do/metasearch.pl?query=inurl:https://grafana.com/grafana/dashboards minio); even the [basic one provided by
MinIO, Inc](https://grafana.com/grafana/dashboards/13502-minio-dashboard/) doesn't work.

We did manage to import [this dashboard from micah](https://grafana.torproject.org/d/pJnnS4hZz-micah/minio-overview-micah?orgId=1&refresh=10s), but it is
currently showing mostly empty graphs. It could be that we don't have
enough metrics yet for the dashboards to operate correctly.

Fortunately, our MinIO server is configured to talk with the
Prometheus server with the [MINIO_PROMETHEUS_URL](https://min.io/docs/minio/linux/reference/minio-server/minio-server.html#envvar.MINIO_PROMETHEUS_URL) variable, which
makes various metrics visible directly in
https://localhost:9090/tools/metrics.

## Tests

To make sure the service still works after an upgrade, you can try
[creating a bucket](#create-a-bucket).

## Logs

The logs from the last boot of the `container-minio.service` can be inspected with:

    journalctl -u container-minio.service -b

MinIO doesn't seem to keep PII in its logs but PII may of course be
recorded in the buckets by the services and users using it. This is
considered not the responsibility of the service.

## Backups

MinIO uses a [storage backend](#storage) that possibly requires the whole
service to be shutdown before backups are made in order for backups to
be consistent.

It is therefore assumed backups are not consistent and a recovery of a
complete loss of a host is difficult or impossible.

This clearly needs to be improved, see the [upstream data recovery
options](https://min.io/docs/minio/linux/operations/data-recovery.html) and [their stance on business continuity](https://blog.min.io/business-continuity-disaster-recovery/).

## Other documentation

 * [Upstream documentation](https://min.io/docs), look for the [Docker documentation](https://min.io/docs/minio/container/index.html)
   and [Linux documentation](https://min.io/docs/minio/linux/index.html)
 * [Troubleshooting options](https://min.io/docs/minio/linux/operations/troubleshooting.html)

# Discussion

## Overview

This project was started in response to growing [large-scale storage
problems](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40478), particularly the need to [host our own GitLab container
registry](https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/89), which culminated in [TPA-RFC-56][]. That RFC discussed
various solutions to the problem and proposed using a single object
storage server running MinIO as a backend to the GitLab registry.

## Security and risk assessment

### Track record

No security audit has been performed on MinIO that we know of.

There's been a [few security vulnerabilities in the past](https://blog.min.io/tag/security-advisory/) but none
published there March 2021. There *is* however a steady stream of
vulnerabilities on [CVE Details](https://www.cvedetails.com/vulnerability-list.php?vendor_id=18671), including an alarming disclosure
of the `MINIO_ROOT_PASSWORD` ([CVE-2023-28432](https://github.com/minio/minio/security/advisories/GHSA-6xvq-wj2x-3h3q)). It seems like
newer vulnerabilities are disclosed through their [GitHub security
page](https://github.com/minio/minio/security).

They only support the latest release, so [automated upgrades](#upgrades) are a
requirement for this project.

### Disclosure risks

There's an inherent risk of bucket disclosure with object storage
APIs. There's been numerous incidents of AWS S3 buckets being leaked
because of improper access policies. We have tried to establish good
practices on this by having scoped users and limited access keys, but
those problems are ultimately in the hands of users, which is
fundamentally why this is such a big problem.

Upstream has a few helpful guides there:

* [MinIO Best Practices - Security and Access Control](https://blog.min.io/s3-security-access-control/)
* [Security checklist](https://min.io/docs/minio/container/operations/checklists/security.html)
* [How to Secure MinIO - Part 1](https://blog.min.io/secure-minio-1/) (and no, there's no "Part 2")

### Audit logs and integrity

MinIO supports [publishing audit logs](https://min.io/docs/minio/container/operations/monitoring/minio-logging.html#minio-logging) to an external server, but
we do not believe this is currently necessary given that most of the
data on the object storage is supposed to be public GitLab data.

MinIO also has many features to ensure [data integrity and
authenticity](https://blog.min.io/data-authenticity-integrity/), namely [erasure coding][], object versioning, and
[immutability][].

[erasure coding]: https://min.io/docs/minio/linux/operations/concepts/erasure-coding.html#minio-erasure-coding
[immutability]: https://min.io/product/data-immutability-for-object-storage

### Port forwarding and container issues

We originally had problems with our container-based configuration as
the `podman run --publish` lines made it impossible to firewall using
our normal tools effectively (see [incident
tpo/tpa/team#41259](https://gitlab.torproject.org/tpo/tpa/team/-/issues/incident/41259)). This was due to the NAT tables created by
podman that were forwarding packets before they were hitting our
normal `INPUT` rules. This made the service globally accessible, while
we actually want to somewhat restrict it, at the very least the
administration interface.

The fix ended up being running the container with relaxed privileges
(`--network=host`). This could also have been worked around by using
an Nginx proxy in front, and upstream has a guide on [how to Use
Nginx, LetsEncrypt and Certbot for Secure Access to MinIO](https://blog.min.io/minio-nginx-letsencrypt-certbot/).

### UNIX user privileges

The container are ran as the `minio` user created by Puppet, using
`podman --user` but not the `User=` directive in the systemd unit. The
latter doesn't work as podman expects a `systemd --user` session, see
also [upstream issue 12778](https://github.com/containers/podman/issues/12778) for that discussion.

### Admin interface access

The admin interface is not currently available to the public. We're
not fully confident that opening up this attack surface is worth it
so, for now, users need to use port forwarding over SSH to access the
admin interface. This is seen as a "lesser evil", but might become
completely impractical if we want users outside TPA to be able to
manage their buckets and so on.

Currently, the user creation procedures and bucket policies should be
good enough to allow public access to the management console, that
said. If we change this policy, a review of the documentation here
will be required, in particular the [interfaces](#interfaces),
[authentication](#authentication) and [Access the admin interface](#access-the-admin-interface) sections.

## Technical debt and next steps

Some of the Puppet configuration could be migrated to a [Puppet
module](#minio-puppet-module), if we're willing to abandon the container strategy and
switch to upstream binaries. This will impact automated upgrades
however. We could also integrate our container strategy in the Puppet
module.

Another big problem with this service is the lack of appropriate
backups, see the [backups section](#backups) for details.

## Proposed Solution

This project was discussed in [TPA-RFC-56][].

## Other alternatives

### Other object storage options

See [TPA-RFC-56][] for a thorough discussion.

### MinIO Puppet module

The [kogitoapp/minio](https://forge.puppet.com/modules/kogitoapp/minio/) provides a way to configure one or many MinIO
servers. Unfortunately it suffers from a set of limitations:

 1. it doesn't support Docker as an install method, only binaries
    (although to its defense it does use a checksum...)

 2. it depends on the [deprecated puppet-certs module](https://github.com/broadinstitute/puppet-certs)

 3. even if it would defend on the newer [puppet-certificates
    module](https://github.com/broadinstitute/puppet-certificates), that module clashes with the way we manage our own
    certificates... we might or might not want to use this module in
    the long term, but right now it seems too big of a jump to follow

 4. it hasn't been updated in about two years (last release in
    September 2021, as of July 2023)

We might still want to consider that module if we expand the fleet to
multiple servers.

### Other object storage clients

In the above guides, we use [rclone][] to talk to the object storage
server, as a generic client, but there are obviously many other
implementations that can talk with cloud providers such as MinIO.

We picked `rclone` because it's packaged in Debian, fast, allows us to
store access keys encrypted, and is generally useful for many other
purposes as well.

Other alternatives include:

 * [s3cmd](https://s3tools.org/s3cmd) and [aws-cli](https://github.com/aws/aws-cli) are both packaged in Debian, but
   unclear if usable on other remotes than the Amazon S3 service
 * [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html) is a Python library that allows one to talk to object
   storage services, presumably not just Amazon S3 as well, [Ruby
   Fog](https://fog.io/) is the same for Ruby, and actually used in GitLab
 * [restic](https://restic.net/) can backup to S3 buckets, and so can other backup tools
   (e.g. on Mac, at least [Arq](https://www.arqbackup.com/), [Cyberduck](https://cyberduck.io/) and [Transmit](https://panic.com/transmit/)
   [apparently](https://help.switch.ch/de/engines/documentation/object-storage/s3-clients-and-libraries/#tab-3f6fdb60-1607-11ec-a1a3-5254009dc73c-3) can)

[rclone]: https://rclone.org/s3/
